<?php
/**
 * Contact Form shortcode
 *
 * @var string $title
 */
?>
<form class="songs__form songs__form--contact">
    <?php wp_nonce_field('wp_rest'); ?>
    <fieldset class="form__fields">
        <?php if ($title): ?>
            <legend class="form__heading">
                <?php esc_html_e($title); ?>
            </legend>
        <?php endif; ?>
        <div class="form__field">
            <label for="songs-form-name" class="field__label">Name:</label>
            <input type="text" id="songs-form-name" name="name" class="field__input" required>
        </div>
        <div class="form__field">
            <label for="songs-form-email" class="field__label">Email:</label>
            <input type="email" id="songs-form-email" name="email" class="field__input field__input--email" required>
        </div>
        <div class="form__field form__field--submit">
            <button>Submit</button>
        </div>
    </fieldset>
</form>
