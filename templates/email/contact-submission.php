<?php
/**
 * New contact submission email
 *
 * @var string $submission_name
 * @var string $submission_email
 * @var int|null $id Post ID
 */

$admin_url = admin_url('/post.php?post=' . (int)$id . '&action=edit');
?>
<p>New contact submission received:</p>
<ul>
    <li>
        <strong>Name:</strong>
        <?php esc_html_e($submission_name); ?>
    </li>
    <li>
        <strong>Email:</strong>
        <?php esc_html_e($submission_email); ?>
    </li>
    <li>
        <strong>WordPress:</strong>
        <?php if ($id): ?>
            <a href="<?php echo esc_url($admin_url); ?>" target="_blank">
                ID #<?php esc_html_e($id); ?>
            </a>
        <?php else: ?>
            <span style="color: red;">Submission could not be saved to database</span>
        <?php endif; ?>
    </li>
</ul>
