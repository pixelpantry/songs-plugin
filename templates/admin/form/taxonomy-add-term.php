<?php
/**
 * Add taxonomy term from a meta box
 * Based on `post_categories_meta_box` core function.
 *
 * @var WP_Taxonomy $taxonomy
 */

$parent_dropdown_args = apply_filters('post_edit_category_parent_dropdown_args', [
    'taxonomy' => $taxonomy->name,
    'hide_empty' => 0,
    'name' => sprintf('new%s_parent', $taxonomy->name),
    'orderby' => 'name',
    'hierarchical' => 1,
    'show_option_none' => sprintf('&mdash; %s &mdash;', $taxonomy->labels->parent_item),
]);

// Variable element attributes
$wrap_id = sprintf('%s-adder', $taxonomy->name);
$link_id = sprintf('%s-add-toggle', $taxonomy->name);
$form_id = sprintf('%s-add', $taxonomy->name);
$input_id = sprintf('new%s', $taxonomy->name);
$input_parent_id = sprintf('new%s_parent', $taxonomy->name);
$button_id = sprintf('%s-add-submit', $taxonomy->name);
$button_data = sprintf('add:%schecklist:%s-add', $taxonomy->name, $taxonomy->name);
$nonce_action = sprintf('add-%s', $taxonomy->name);
$nonce_name = sprintf('_ajax_nonce-add-%s', $taxonomy->name);
?>
<div id="<?php esc_html_e($wrap_id); ?>" class="wp-hidden-children">
    <a href="#<?php esc_html_e($form_id); ?>"
       id="<?php esc_html_e($link_id); ?>"
       class="hide-if-no-js taxonomy-add-new">
        <?php printf(__('+ %s'), $taxonomy->labels->add_new_item); ?>
    </a>
    <p id="<?php esc_attr_e($form_id); ?>" class="category-add wp-hidden-child">
        <label class="screen-reader-text" for="<?php esc_attr_e($input_id); ?>">
            <?php echo $taxonomy->labels->add_new_item; ?>
        </label>
        <input type="text"
               id="<?php esc_attr_e($input_id); ?>"
               name="<?php esc_attr_e($input_id); ?>"
               value="<?php echo esc_attr($taxonomy->labels->new_item_name); ?>"
               class="form-required form-input-tip"
               aria-required="true">
        <label class="screen-reader-text" for="<?php esc_attr_e($input_parent_id); ?>">
            <?php echo $taxonomy->labels->parent_item_colon; ?>
        </label>
        <?php wp_dropdown_categories($parent_dropdown_args); ?>
        <input type="button"
               id="<?php esc_attr_e($button_id); ?>"
               value="<?php echo esc_attr($taxonomy->labels->add_new_item); ?>"
               class="button category-add-submit"
               data-wp-lists="<?php esc_attr_e($button_data); ?>">
        <?php wp_nonce_field($nonce_action, $nonce_name, false); ?>
        <span id="<?php echo $taxonomy->name; ?>-ajax-response"></span>
    </p>
</div>
