<?php
/**
 * Radio list of taxonomy terms
 * Based on `post_categories_meta_box` core function.
 *
 * @var WP_Taxonomy $taxonomy
 * @var WP_Post $post
 */

use PixelPantry\Songs\Walker\TaxonomyRadioListWalker;
use PixelPantry\Songs\Utils\Template;

$terms = get_terms([
    'taxonomy' => $taxonomy->name,
    'hide_empty' => false,
]);

// Variable element attributes
$wrap_id = sprintf('taxonomy-%s', $taxonomy->name);
$list_id = sprintf('%schecklist', $taxonomy->name);
$list_data = sprintf('list:%s', $taxonomy->name);
$input_name = sprintf('tax_input[%s][]', $taxonomy->name);
?>
<div id="<?php esc_attr_e($wrap_id); ?>" class="categorydiv">
    <div class="tabs-panel">
        <input type="hidden" name="<?php esc_attr_e($input_name); ?>" value="0">
        <ul id="<?php esc_attr_e($list_id); ?>"
            class="categorychecklist form-no-clear"
            data-wp-lists="<?php esc_attr_e($list_data); ?>">
            <?php
            wp_terms_checklist(
                $post->ID,
                [
                    'taxonomy' => $taxonomy->name,
                    'walker' => new TaxonomyRadioListWalker,
                ]
            );
            ?>
        </ul>
    </div>
    <?php if (current_user_can($taxonomy->cap->edit_terms)): ?>
        <?php Template::render('admin/form/taxonomy-add-term', ['taxonomy' => $taxonomy]); ?>
    <?php endif; ?>
</div>
