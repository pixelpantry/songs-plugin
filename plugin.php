<?php
/**
 * Plugin Name: Songs Plugin
 * Plugin URI: https://bitbucket.org/pixelpantry/songs-plugin
 * Description: Songs Plugin by Pixel Pantry
 * Author: Pixel Pantry
 * Version: 0.2.0
 * Author URI: https://pixelpantry.com.au/
 */

// Non-composer installation of plugin requires autoloader
if (file_exists(__DIR__ . '/vendor/autoload.php')) {
    require_once 'vendor/autoload.php';
}

if (!class_exists('PixelPantry\Songs\Plugin')) {
    return;
}

use PixelPantry\Songs\Plugin;

new Plugin();
