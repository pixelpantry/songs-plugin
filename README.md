# Songs Plugin

[Songs plugin](https://bitbucket.org/pixelpantry/songs-plugin)
by [Pixel Pantry](https://pixelpantry.com.au).

## Table of Contents

- [Getting Started](#markdown-header-1-getting-started)
- [Project Structure](#markdown-header-2-project-structure)
- [Composer Installation](#markdown-header-3-composer-installation)

## 1. Getting Started

To install this plugin and developer further:

1. Move the plugin to `wp-content/plugins/songs-plugin`
2. Install dependencies with:

```
$ composer install
```

3. Activate the plugin (e.g. `wp plugin activate songs-plugin`)

You can alternatively [install this plugin using composer](#markdown-header-3-composer-installation) which does not require the above installation steps.

NOTE: minimum PHP v8.0 and WordPress v5.9

## 2. Project Structure

```
.
├─ .editorconfig
├─ .gitignore
├─ CHANGELOG.md
├─ composer.json
├─ composer.lock
├─ LICENSE
├─ plugin.php       # Plugin entry point
├─ README.md
├─ src              # Classes
└─ templates        # Templates
```

## 3. Composer Installation

Plugin is able to be installed using Composer with the following steps:

1. Setup WordPress to work with composer (e.g.
   using [Bedrock](https://roots.io/bedrock/), or with
   a [composer/installers](https://github.com/composer/installers) setup)
2. Add the plugin repository to your `composer.json` files. E.g.

```
  "repositories": [
    {
      "type": "vcs",
      "url": "https://pixelpantry@bitbucket.org/pixelpantry/songs-plugin.git"
    },
    ...
  ]
```

3. Run `composer require pixelpantry/songs-plugin` to install the plugin
4. Activate the plugin (e.g. `wp plugin activate songs-plugin`)
