/**
 * Basic form submission / REST code
 *
 * NOTES:
 *  - I would typically run a webpack or gulp script to bundle/prepare dist assets
 *  - Babel to transpile newer JS for older browser support
 *  - NPM for package manager
 *  - Polyfills
 *  - For a quick and straight-forward frontend I might use default WP jQuery opposed to vanilla JS
 *  - etc.
 */

window.addEventListener('DOMContentLoaded', (event) => {
  const forms = document.querySelectorAll('.songs__form--contact');

  forms.forEach(form => {
    form.addEventListener('submit', onSubmitCallback);
  });
});

function onSubmitCallback(event) {
  const form = this;

  fetch(`${songs.api_base}/contact`, {
    method: 'POST',
    headers: getFormHeaders(form),
    body: getFormData(form),
  })
    .then(response => onFormResponse(response))
    .then(data => onFormSuccess(form, data))
    .catch(error => onFormError(error));

  event.preventDefault();
}

const getFormHeaders = form => {
  const nonce = form.querySelector('[name=_wpnonce]').value;

  return {
    'Content-Type': 'application/json',
    'X-WP-Nonce': nonce,
  }
}

const getFormData = form => {
  const name = form.querySelector('[name=name]').value;
  const email = form.querySelector('[name=email]').value;

  return JSON.stringify({name, email})
}

const onFormResponse = response => {
  return response.json().then(data => {
    if (!response.ok) {
      throw data;
    }

    return data;
  });
}

const onFormSuccess = (form, data) => {
  const {
    message = 'Thank you for your submission.',
  } = data;

  const messageElement = document.createElement('p');
  messageElement.innerHTML = message;

  form.replaceWith(messageElement);
}

const onFormError = error => {
  const {
    message = 'Something went wrong. Please try again later.',
  } = error;

  alert(message);
}
