## 0.2.0 (April 3, 2022)

* Added `[songs-contact]` shortcode with form and API
* Enabled local and composer based plugin installation
* Added instructions to install plugin using composer

## 0.1.0 (April 2, 2022)

* Initial release
* Includes a Songs post type and Genre custom taxonomy
