<?php

namespace PixelPantry\Songs\Walker;

use Walker_Category_Checklist;

class TaxonomyRadioListWalker extends Walker_Category_Checklist
{
    /**
     * Overwrite the `start_el` method of `Walker_Category_Checklist` to output radios instead of checkboxes.
     * This is a copy/paste of original method with minor changes.
     *
     * @see Walker_Category_Checklist::start_el()
     */
    public function start_el(&$output, $category, $depth = 0, $args = array(), $current_object_id = 0)
    {
        if (empty($args['taxonomy'])) {
            $taxonomy = 'category';
        } else {
            $taxonomy = $args['taxonomy'];
        }

        if ('category' === $taxonomy) {
            $name = 'post_category';
        } else {
            $name = 'tax_input[' . $taxonomy . ']';
        }

        $args['popular_cats'] = !empty($args['popular_cats'])
            ? array_map('intval', $args['popular_cats'])
            : array();

        $class = in_array($category->term_id, $args['popular_cats'], true)
            ? ' class="popular-category"'
            : '';

        $args['selected_cats'] = !empty($args['selected_cats'])
            ? array_map('intval', $args['selected_cats'])
            : array();

        if (!empty($args['list_only'])) {
            $aria_checked = 'false';
            $inner_class = 'category';

            if (in_array($category->term_id, $args['selected_cats'], true)) {
                $inner_class .= ' selected';
                $aria_checked = 'true';
            }

            $output .= "\n" . '<li' . $class . '>' .
                '<div class="' . $inner_class . '" data-term-id=' . $category->term_id .
                ' tabindex="0" role="radio" aria-checked="' . $aria_checked . '">' .
                esc_html(apply_filters('the_category', $category->name, '', '')) . '</div>';
        } else {
            $is_selected = in_array($category->term_id, $args['selected_cats'], true);
            $is_disabled = !empty($args['disabled']);

            $output .= "\n<li id='{$taxonomy}-{$category->term_id}'$class>" .
                '<label class="selectit"><input value="' . $category->term_id . '" type="radio" name="' . $name . '[]" id="in-' . $taxonomy . '-' . $category->term_id . '"' .
                checked($is_selected, true, false) .
                disabled($is_disabled, true, false) . ' /> ' .
                esc_html(apply_filters('the_category', $category->name, '', '')) . '</label>';
        }
    }
}
