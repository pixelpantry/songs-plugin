<?php

namespace PixelPantry\Songs\API;

use WP_REST_Request;

class Permission
{
    public static function canPostPublicRoute(WP_REST_Request $request): bool
    {
        // Anyone can POST to a public route.
        // Routes can be restricted with `current_user_can` when using wp_rest nonce.
        return true;
    }
}
