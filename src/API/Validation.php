<?php

namespace PixelPantry\Songs\API;

use WP_REST_Request;
use WP_Error;

class Validation
{
    public static function email(mixed $param, WP_REST_Request $request, string $key): bool|WP_Error
    {
        if (!is_email($param)) {
            return new WP_Error(
                'invalid_email',
                'Please enter a valid email.'
            );
        }

        return true;
    }

    public static function name(mixed $param, WP_REST_Request $request, string $key): bool|WP_Error
    {
        if (strlen(trim($param)) === 0) {
            return new WP_Error(
                'invalid_name',
                'Please enter a valid name.'
            );
        }

        return true;
    }
}
