<?php

namespace PixelPantry\Songs\API\Route;

use PixelPantry\Songs\Utils\Config;
use PixelPantry\Songs\Utils\Post;
use PixelPantry\Songs\Utils\Mailer;
use PixelPantry\Songs\API\Permission;
use PixelPantry\Songs\API\Validation;
use WP_REST_Server;
use WP_REST_Request;
use WP_REST_Response;
use WP_Error;

class Contact
{
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'registerRoutes']);
    }

    public function registerRoutes()
    {
        register_rest_route(Config::getApiBase(), '/contact', [
            'methods' => WP_REST_Server::CREATABLE,
            'callback' => [$this, 'contactRoute'],
            'permission_callback' => [Permission::class, 'canPostPublicRoute'],
            'args' => [
                'name' => [
                    'required' => true,
                    'validate_callback' => [Validation::class, 'name'],
                ],
                'email' => [
                    'required' => true,
                    'validate_callback' => [Validation::class, 'email'],
                ],
            ],
        ]);
    }

    public function contactRoute(WP_REST_Request $request): WP_REST_Response|WP_Error
    {
        $name = $request->get_param('name');
        $email = $request->get_param('email');

        $id = $this->storeSubmission($name, $email);
        $this->sendAdminEmail($name, $email, $id);

        if (!$id) {
            return new WP_Error(
                'submission_failed',
                'Something went wrong. Please try again later.',
            );
        }

        return new WP_REST_Response(['response' => 'Thank you for your submission.']);
    }

    private function storeSubmission(string $name, string $email): ?int
    {
        return Post::create([
            'post_type' => 'songs-contact',
            'post_status' => 'draft',
            'post_title' => esc_attr($name),
            'post_content' => sprintf("Name: %s\nEmail: %s", esc_attr($name), esc_attr($email)),
        ]);
    }

    private function sendAdminEmail(string $name, string $email, ?int $id): bool
    {
        $admin = get_option('admin_email');

        return Mailer::send($admin, 'New Contact Submission', 'contact-submission', [
            'submission_name' => $name,
            'submission_email' => $email,
            'id' => $id,
        ]);
    }
}
