<?php

namespace PixelPantry\Songs;

class Roles
{
    private const ADMIN_CAPABILITIES = [
        // Songs post type
        'edit_song',
        'read_song',
        'delete_song',
        'edit_songs',
        'edit_others_songs',
        'publish_songs',
        'read_private_songs',
        'edit_songs',

        // Genre taxonomy
        'manage_genres',
        'edit_genres',
        'delete_genres',
        'assign_genres',
    ];

    public function addAdminCapabilities(): void
    {
        $role = get_role('administrator');

        foreach (self::ADMIN_CAPABILITIES as $capability) {
            $role->add_cap($capability);
        }
    }

    public function removeAdminCapabilities(): void
    {
        $role = get_role('administrator');

        foreach (self::ADMIN_CAPABILITIES as $capability) {
            $role->remove_cap($capability);
        }
    }
}
