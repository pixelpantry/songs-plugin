<?php

namespace PixelPantry\Songs;

class Deactivate
{
    public static function run(): void
    {
        self::deactivateRoles();
    }

    private static function deactivateRoles(): void
    {
        $roles = new Roles();
        $roles->removeAdminCapabilities();
    }
}
