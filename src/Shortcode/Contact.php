<?php

namespace PixelPantry\Songs\Shortcode;

use PixelPantry\Songs\Utils\Template;

/**
 * [songs-contact] shortcode
 *
 * Example usage:
 * (a) [songs-contact]
 * (b) [songs-contact title="Get in touch"]
 */
class Contact
{
    public function __construct()
    {
        add_shortcode('songs-contact', [$this, 'compile']);
    }

    public function compile($attributes, $content = null): string
    {
        $defaults = [
            'title' => 'Contact',
        ];

        $context = shortcode_atts($defaults, $attributes);

        return Template::compile('shortcode/contact', $context);
    }
}
