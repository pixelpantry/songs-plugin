<?php

namespace PixelPantry\Songs\Utils;

class Mailer
{
    public static function setContentType(): string
    {
        return 'text/html';
    }

    public static function setFrom(): string
    {
        return Config::getMailFromAddress();
    }

    public static function setFromName(): string
    {
        return Config::getMailFromName();
    }

    public static function send(string $to, string $subject, string $template, array $params = []): bool
    {
        add_filter('wp_mail_content_type', [self::class, 'setContentType']);
        add_filter('wp_mail_from', [self::class, 'setFrom']);
        add_filter('wp_mail_from_name', [self::class, 'setFromName']);

        $body = Template::compile('email/' . $template, $params);
        $mail = wp_mail($to, $subject, $body);

        remove_filter('wp_mail_content_type', [self::class, 'setContentType']);
        remove_filter('wp_mail_from', [self::class, 'setFrom']);
        remove_filter('wp_mail_from_name', [self::class, 'setFromName']);

        // Some SMTP plugins overwrite `wp_mail` and don't return bool.
        // This return keeps us type safe.
        return $mail === true;
    }
}
