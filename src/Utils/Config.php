<?php

namespace PixelPantry\Songs\Utils;

class Config
{
    public static function getPluginDir(): string
    {
        return plugin_dir_path(dirname(__FILE__, 2));
    }

    public static function getPluginEntryFile(): string
    {
        return self::getPluginDir() . 'plugin.php';
    }

    public static function getTemplateDir(): string
    {
        return self::getPluginDir() . 'templates';
    }

    public static function getPluginUrl(): string
    {
        return plugin_dir_url(dirname(__FILE__, 2));
    }

    public static function getMailFromAddress(): string
    {
        $url = parse_url(get_bloginfo('url'));

        return sprintf('mailer@%s', $url['host']);
    }

    public static function getMailFromName(): string
    {
        return 'Admin';
    }

    public static function getApiBase(): string
    {
        return 'songs/v1';
    }
}
