<?php

namespace PixelPantry\Songs\Utils;

class Template
{
    /**
     * Print a rendered template
     *
     * @param string $name Name of template
     * @param array $params Parameters to pass to template
     * @return void
     */
    public static function render(string $name, array $params = []): void
    {
        echo self::compile($name, $params) ?: '';
    }

    /**
     * Return a rendered template
     *
     * @param string $name Name of template
     * @param array $params Parameters to pass to template
     * @return string|null
     */
    public static function compile(string $name, array $params = []): ?string
    {
        $template = sprintf('%s/%s.php', Config::getTemplateDir(), $name);

        if (!is_file($template) || !is_readable($template)) {
            return null;
        }

        extract($params, EXTR_SKIP);
        ob_start();
        include $template;
        return ob_get_clean();
    }
}
