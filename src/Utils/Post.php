<?php

namespace PixelPantry\Songs\Utils;

use WP_Post;
use WP_Error;

/**
 * Post CRUD helper class
 *
 * For more complex requirements consider creating
 * abstract Post Entity/Repository/Factory classes and
 * extending as needed for different custom post types.
 */
class Post
{
    public static function get(int $id): ?WP_Post
    {
        return get_post($id);
    }

    public static function create(array $data): ?int
    {
        $id = wp_insert_post($data);

        if ($id instanceof WP_Error) {
            return null;
        }

        return $id;
    }

    public static function delete(int $id): bool
    {
        $deletedPost = wp_delete_post($id, true);

        if (!$deletedPost instanceof WP_Post) {
            return false;
        }

        return true;
    }
}
