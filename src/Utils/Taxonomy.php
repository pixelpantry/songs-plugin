<?php

namespace PixelPantry\Songs\Utils;

use WP_Post;
use WP_Taxonomy;

class Taxonomy
{
    public static function radioListMetaBox(WP_Post $post, array $box): void
    {
        $args = $box['args'] ?? null;
        $taxonomy = $args['taxonomy']
            ? get_taxonomy($args['taxonomy'])
            : null;

        if (!$taxonomy instanceof WP_Taxonomy) {
            return;
        }

        Template::render('admin/meta-box/taxonomy-radio-list', [
            'taxonomy' => $taxonomy,
            'post' => $post,
        ]);
    }
}
