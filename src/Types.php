<?php

namespace PixelPantry\Songs;

use PixelPantry\Songs\Utils\Taxonomy;
use PixelPantry\Songs\Walker\TaxonomyRadioListWalker;

class Types
{
    public function __construct()
    {
        add_action('init', [$this, 'registerPostTypes']);
        add_action('init', [$this, 'registerTaxonomies']);
        add_filter('wp_terms_checklist_args', [$this, 'setGenreTaxonomyWalker']);
    }

    public function registerPostTypes(): void
    {
        register_post_type('songs', [
            'public' => true,
            'label' => 'Songs',
            'menu_icon' => 'dashicons-format-audio',
            'has_archive' => 'songs',
            'taxonomies' => ['genre'],
            'rewrite' => [
                'slug' => 'song',
            ],
            'capabilities' => [
                'edit_post' => 'edit_song',
                'read_post' => 'read_song',
                'delete_post' => 'delete_song',
                'edit_posts' => 'edit_songs',
                'edit_others_posts' => 'edit_others_songs',
                'publish_posts' => 'publish_songs',
                'read_private_posts' => 'read_private_songs',
                'create_posts' => 'edit_songs',
            ],
        ]);

        register_post_type('songs-contact', [
            'public' => false,
            'show_ui' => true,
            'label' => 'Contact',
            'menu_icon' => 'dashicons-email',
        ]);
    }

    public function registerTaxonomies(): void
    {
        register_taxonomy('genre', ['songs'], [
            'label' => 'Genre',
            'public' => true,
            'meta_box_cb' => [Taxonomy::class, 'radioListMetaBox'],
            'meta_box_sanitize_cb' => 'taxonomy_meta_box_sanitize_cb_checkboxes',
            'default_term' => [
                'name' => 'Classical',
                'slug' => 'classical',
                'description' => 'Greatest classical hits',
            ],
            'capabilities' => [
                'manage_terms' => 'manage_genres',
                'edit_terms' => 'edit_genres',
                'delete_terms' => 'delete_genres',
                'assign_terms' => 'assign_genres',
            ],
        ]);
    }

    public function setGenreTaxonomyWalker(array|string $args): array
    {
        $args = wp_parse_args($args);
        $taxonomy = $args['taxonomy'] ?? null;

        if ($taxonomy !== 'genre') {
            return $args;
        }

        $args['walker'] = new TaxonomyRadioListWalker;
        return $args;
    }
}
