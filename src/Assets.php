<?php

namespace PixelPantry\Songs;

use PixelPantry\Songs\Utils\Config;

class Assets
{
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);
    }

    public function enqueueScripts(): void
    {
        wp_enqueue_script('songs-js', Config::getPluginURL() . '/assets/main.js', [], '0.2.0', true);
        wp_localize_script('songs-js', 'songs', [
            'api_base' => get_rest_url() . Config::getApiBase(),
        ]);
    }
}
