<?php

namespace PixelPantry\Songs;

use PixelPantry\Songs\Utils\Config;

class Plugin
{
    public function __construct()
    {
        new Types();
        new Shortcode();
        new API();
        new Assets();

        $this->setupActivation();
        $this->setupDeactivation();
    }

    private function setupActivation(): void
    {
        register_activation_hook(Config::getPluginEntryFile(), [Activate::class, 'run']);
    }

    private function setupDeactivation(): void
    {
        register_deactivation_hook(Config::getPluginEntryFile(), [Deactivate::class, 'run']);
    }
}
