<?php

namespace PixelPantry\Songs;

class Activate
{
    public static function run(): void
    {
        self::activateRoles();
    }

    private static function activateRoles(): void
    {
        $roles = new Roles();
        $roles->addAdminCapabilities();
    }
}
